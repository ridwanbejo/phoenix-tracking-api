# TrackingAPI

To start your Phoenix server:

  * Create user database on postgres with username `phoenix` and password `phoenix`
  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.create && mix ecto.migrate`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000/gps_tracking) from your browser or HTTP Client.

  * Listing Items: **GET** `localhost:4000/api/gps_tracking/`
  * Get Item: **GET** `localhost:4000/api/gps_tracking/<:id>`
  * Create Item: **POST** `localhost:4000/api/gps_tracking/` with this sample payload:

```
{ 
  "gps_tracking":{
      "application_id": "7488a646-e31f-11e4-aace-600308960662",
      "user_id": "7488a646-adsf-55eg-abcd-600308960662",
      "latitude": "-6.914744",
      "longitude": "107.609810",
      "created_at": "2018-09-10T09:00:00.000000Z"
  }
}
```

  * Delete Item: **DELETE** `localhost:4000/api/gps_tracking/<:id>`

Ready to run in production? Please [check our deployment guides](http://www.phoenixframework.org/docs/deployment).

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: http://phoenixframework.org/docs/overview
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix
