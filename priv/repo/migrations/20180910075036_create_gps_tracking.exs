defmodule TrackingAPI.Repo.Migrations.CreateGpsTracking do
  use Ecto.Migration

  def change do
    create table(:gps_tracking) do
      add :application_id, :uuid
      add :user_id, :uuid
      add :latitude, :string
      add :longitude, :string
      add :created_at, :utc_datetime

      timestamps()
    end

  end
end
