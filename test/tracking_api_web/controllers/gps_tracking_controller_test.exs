defmodule TrackingAPIWeb.GPSTrackingControllerTest do
  use TrackingAPIWeb.ConnCase

  alias TrackingAPI.Tracking
  alias TrackingAPI.Tracking.GPSTracking

  @create_attrs %{application_id: "7488a646-e31f-11e4-aace-600308960662", created_at: "2010-04-17 14:00:00.000000Z", latitude: "some latitude", longitude: "some longitude", user_id: "7488a646-e31f-11e4-aace-600308960662"}
  @update_attrs %{application_id: "7488a646-e31f-11e4-aace-600308960668", created_at: "2011-05-18 15:01:01.000000Z", latitude: "some updated latitude", longitude: "some updated longitude", user_id: "7488a646-e31f-11e4-aace-600308960662"}
  @invalid_attrs %{application_id: nil, created_at: nil, latitude: nil, longitude: nil, user_id: nil}

  def fixture(:gps_tracking) do
    {:ok, gps_tracking} = Tracking.create_gps_tracking(@create_attrs)
    gps_tracking
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all gps_tracking", %{conn: conn} do
      conn = get conn, gps_tracking_path(conn, :index)
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create gps_tracking" do
    test "renders gps_tracking when data is valid", %{conn: conn} do
      conn = post conn, gps_tracking_path(conn, :create), gps_tracking: @create_attrs
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get conn, gps_tracking_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id,
        "application_id" => "7488a646-e31f-11e4-aace-600308960662",
        "created_at" => "2010-04-17T14:00:00.000000Z",
        "latitude" => "some latitude",
        "longitude" => "some longitude",
        "user_id" => "7488a646-e31f-11e4-aace-600308960662"}
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, gps_tracking_path(conn, :create), gps_tracking: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update gps_tracking" do
    setup [:create_gps_tracking]

    test "renders gps_tracking when data is valid", %{conn: conn, gps_tracking: %GPSTracking{id: id} = gps_tracking} do
      conn = put conn, gps_tracking_path(conn, :update, gps_tracking), gps_tracking: @update_attrs
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get conn, gps_tracking_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id,
        "application_id" => "7488a646-e31f-11e4-aace-600308960668",
        "created_at" => "2011-05-18T15:01:01.000000Z",
        "latitude" => "some updated latitude",
        "longitude" => "some updated longitude",
        "user_id" => "7488a646-e31f-11e4-aace-600308960662"}
    end

    test "renders errors when data is invalid", %{conn: conn, gps_tracking: gps_tracking} do
      conn = put conn, gps_tracking_path(conn, :update, gps_tracking), gps_tracking: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete gps_tracking" do
    setup [:create_gps_tracking]

    test "deletes chosen gps_tracking", %{conn: conn, gps_tracking: gps_tracking} do
      conn = delete conn, gps_tracking_path(conn, :delete, gps_tracking)
      assert response(conn, 204)
      assert_error_sent 404, fn ->
        get conn, gps_tracking_path(conn, :show, gps_tracking)
      end
    end
  end

  defp create_gps_tracking(_) do
    gps_tracking = fixture(:gps_tracking)
    {:ok, gps_tracking: gps_tracking}
  end
end
