defmodule TrackingAPI.TrackingTest do
  use TrackingAPI.DataCase

  alias TrackingAPI.Tracking

  describe "gps_tracking" do
    alias TrackingAPI.Tracking.GPSTracking

    @valid_attrs %{application_id: "7488a646-e31f-11e4-aace-600308960662", created_at: "2010-04-17 14:00:00.000000Z", latitude: "some latitude", longitude: "some longitude", user_id: "7488a646-e31f-11e4-aace-600308960662"}
    @update_attrs %{application_id: "7488a646-e31f-11e4-aace-600308960668", created_at: "2011-05-18 15:01:01.000000Z", latitude: "some updated latitude", longitude: "some updated longitude", user_id: "7488a646-e31f-11e4-aace-600308960662"}
    @invalid_attrs %{application_id: nil, created_at: nil, latitude: nil, longitude: nil, user_id: nil}

    def gps_tracking_fixture(attrs \\ %{}) do
      {:ok, gps_tracking} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Tracking.create_gps_tracking()

      gps_tracking
    end

    test "list_gps_tracking/0 returns all gps_tracking" do
      gps_tracking = gps_tracking_fixture()
      assert Tracking.list_gps_tracking() == [gps_tracking]
    end

    test "get_gps_tracking!/1 returns the gps_tracking with given id" do
      gps_tracking = gps_tracking_fixture()
      assert Tracking.get_gps_tracking!(gps_tracking.id) == gps_tracking
    end

    test "create_gps_tracking/1 with valid data creates a gps_tracking" do
      assert {:ok, %GPSTracking{} = gps_tracking} = Tracking.create_gps_tracking(@valid_attrs)
      assert gps_tracking.application_id == "7488a646-e31f-11e4-aace-600308960662"
      assert gps_tracking.created_at == DateTime.from_naive!(~N[2010-04-17 14:00:00.000000Z], "Etc/UTC")
      assert gps_tracking.latitude == "some latitude"
      assert gps_tracking.longitude == "some longitude"
      assert gps_tracking.user_id == "7488a646-e31f-11e4-aace-600308960662"
    end

    test "create_gps_tracking/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Tracking.create_gps_tracking(@invalid_attrs)
    end

    test "update_gps_tracking/2 with valid data updates the gps_tracking" do
      gps_tracking = gps_tracking_fixture()
      assert {:ok, gps_tracking} = Tracking.update_gps_tracking(gps_tracking, @update_attrs)
      assert %GPSTracking{} = gps_tracking
      assert gps_tracking.application_id == "7488a646-e31f-11e4-aace-600308960668"
      assert gps_tracking.created_at == DateTime.from_naive!(~N[2011-05-18 15:01:01.000000Z], "Etc/UTC")
      assert gps_tracking.latitude == "some updated latitude"
      assert gps_tracking.longitude == "some updated longitude"
      assert gps_tracking.user_id == "7488a646-e31f-11e4-aace-600308960662"
    end

    test "update_gps_tracking/2 with invalid data returns error changeset" do
      gps_tracking = gps_tracking_fixture()
      assert {:error, %Ecto.Changeset{}} = Tracking.update_gps_tracking(gps_tracking, @invalid_attrs)
      assert gps_tracking == Tracking.get_gps_tracking!(gps_tracking.id)
    end

    test "delete_gps_tracking/1 deletes the gps_tracking" do
      gps_tracking = gps_tracking_fixture()
      assert {:ok, %GPSTracking{}} = Tracking.delete_gps_tracking(gps_tracking)
      assert_raise Ecto.NoResultsError, fn -> Tracking.get_gps_tracking!(gps_tracking.id) end
    end

    test "change_gps_tracking/1 returns a gps_tracking changeset" do
      gps_tracking = gps_tracking_fixture()
      assert %Ecto.Changeset{} = Tracking.change_gps_tracking(gps_tracking)
    end
  end
end
