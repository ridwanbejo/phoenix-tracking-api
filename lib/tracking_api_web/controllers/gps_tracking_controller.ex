defmodule TrackingAPIWeb.GPSTrackingController do
  use TrackingAPIWeb, :controller

  alias TrackingAPI.Tracking
  alias TrackingAPI.Tracking.GPSTracking

  action_fallback TrackingAPIWeb.FallbackController

  def index(conn, _params) do
    gps_tracking = Tracking.list_gps_tracking()
    render(conn, "index.json", gps_tracking: gps_tracking)
  end

  def create(conn, %{"gps_tracking" => gps_tracking_params}) do
    with {:ok, %GPSTracking{} = gps_tracking} <- Tracking.create_gps_tracking(gps_tracking_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", gps_tracking_path(conn, :show, gps_tracking))
      |> render("show.json", gps_tracking: gps_tracking)
    end
  end

  def show(conn, %{"id" => id}) do
    gps_tracking = Tracking.get_gps_tracking!(id)
    render(conn, "show.json", gps_tracking: gps_tracking)
  end

  def update(conn, %{"id" => id, "gps_tracking" => gps_tracking_params}) do
    gps_tracking = Tracking.get_gps_tracking!(id)

    with {:ok, %GPSTracking{} = gps_tracking} <- Tracking.update_gps_tracking(gps_tracking, gps_tracking_params) do
      render(conn, "show.json", gps_tracking: gps_tracking)
    end
  end

  def delete(conn, %{"id" => id}) do
    gps_tracking = Tracking.get_gps_tracking!(id)
    with {:ok, %GPSTracking{}} <- Tracking.delete_gps_tracking(gps_tracking) do
      send_resp(conn, :no_content, "")
    end
  end
end
