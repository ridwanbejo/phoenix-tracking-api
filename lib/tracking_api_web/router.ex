defmodule TrackingAPIWeb.Router do
  use TrackingAPIWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", TrackingAPIWeb do
    pipe_through :api
    resources "/gps_tracking", GPSTrackingController, except: [:edit]
  end
end
