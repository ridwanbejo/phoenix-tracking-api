defmodule TrackingAPIWeb.GPSTrackingView do
  use TrackingAPIWeb, :view
  alias TrackingAPIWeb.GPSTrackingView

  def render("index.json", %{gps_tracking: gps_tracking}) do
    %{data: render_many(gps_tracking, GPSTrackingView, "gps_tracking.json")}
  end

  def render("show.json", %{gps_tracking: gps_tracking}) do
    %{data: render_one(gps_tracking, GPSTrackingView, "gps_tracking.json")}
  end

  def render("gps_tracking.json", %{gps_tracking: gps_tracking}) do
    %{id: gps_tracking.id,
      application_id: gps_tracking.application_id,
      user_id: gps_tracking.user_id,
      latitude: gps_tracking.latitude,
      longitude: gps_tracking.longitude,
      created_at: gps_tracking.created_at}
  end
end
