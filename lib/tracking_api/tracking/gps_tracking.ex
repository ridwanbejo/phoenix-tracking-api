defmodule TrackingAPI.Tracking.GPSTracking do
  use Ecto.Schema
  import Ecto.Changeset


  schema "gps_tracking" do
    field :application_id, Ecto.UUID
    field :created_at, :utc_datetime
    field :latitude, :string
    field :longitude, :string
    field :user_id, Ecto.UUID

    timestamps()
  end

  @doc false
  def changeset(gps_tracking, attrs) do
    gps_tracking
    |> cast(attrs, [:application_id, :user_id, :latitude, :longitude, :created_at])
    |> validate_required([:application_id, :user_id, :latitude, :longitude, :created_at])
  end
end
