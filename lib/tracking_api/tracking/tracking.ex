defmodule TrackingAPI.Tracking do
  @moduledoc """
  The Tracking context.
  """

  import Ecto.Query, warn: false
  alias TrackingAPI.Repo

  alias TrackingAPI.Tracking.GPSTracking

  @doc """
  Returns the list of gps_tracking.

  ## Examples

      iex> list_gps_tracking()
      [%GPSTracking{}, ...]

  """
  def list_gps_tracking do
    Repo.all(GPSTracking)
  end

  @doc """
  Gets a single gps_tracking.

  Raises `Ecto.NoResultsError` if the Gps tracking does not exist.

  ## Examples

      iex> get_gps_tracking!(123)
      %GPSTracking{}

      iex> get_gps_tracking!(456)
      ** (Ecto.NoResultsError)

  """
  def get_gps_tracking!(id), do: Repo.get!(GPSTracking, id)

  @doc """
  Creates a gps_tracking.

  ## Examples

      iex> create_gps_tracking(%{field: value})
      {:ok, %GPSTracking{}}

      iex> create_gps_tracking(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_gps_tracking(attrs \\ %{}) do
    %GPSTracking{}
    |> GPSTracking.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a gps_tracking.

  ## Examples

      iex> update_gps_tracking(gps_tracking, %{field: new_value})
      {:ok, %GPSTracking{}}

      iex> update_gps_tracking(gps_tracking, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_gps_tracking(%GPSTracking{} = gps_tracking, attrs) do
    gps_tracking
    |> GPSTracking.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a GPSTracking.

  ## Examples

      iex> delete_gps_tracking(gps_tracking)
      {:ok, %GPSTracking{}}

      iex> delete_gps_tracking(gps_tracking)
      {:error, %Ecto.Changeset{}}

  """
  def delete_gps_tracking(%GPSTracking{} = gps_tracking) do
    Repo.delete(gps_tracking)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking gps_tracking changes.

  ## Examples

      iex> change_gps_tracking(gps_tracking)
      %Ecto.Changeset{source: %GPSTracking{}}

  """
  def change_gps_tracking(%GPSTracking{} = gps_tracking) do
    GPSTracking.changeset(gps_tracking, %{})
  end
end
